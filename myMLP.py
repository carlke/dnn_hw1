import theano
import theano.tensor as T
import numpy as np
import model
import sys
import utils

from time import time
from sklearn.cross_validation import StratifiedKFold
from sklearn.utils import shuffle

if len(sys.argv) > 3:
    print("Usage: python myMLP.py input_name model_save_name")
    exit(0)
elif len(sys.argv) == 3:
    model_save_name = sys.argv[2]
elif len(sys.argv) == 2:
    model_save_name = None
else:
    print("Usage: python myMLP.py input_name model_save_name")
    exit(0)

print('Loading data...')
t = time()
feature, label, n2lab, frameid = utils.loadData(sys.argv[1])

feature, label, frameid = utils.genFrameWindow(
    feature, label, frameid, window_size=2
)
print('Loaded. Time elapsed: %.2f secs' % (time()-t))


# split into 5-folds train, test sets
folds = StratifiedKFold(label, n_folds=10)
fold_cnt = 0
for train_index, test_index in folds:
    print('Fold %d - train/test size: (%d, %d)' % (fold_cnt+1, len(train_index), len(test_index)))

    fold_cnt += 1

    _x_train, _x_test = feature[train_index], feature[test_index]
    _y_train, _y_test = label[train_index], label[test_index]
    _x_train, _y_train = shuffle(_x_train, _y_train)
    x_train = theano.shared(np.asarray(_x_train, dtype='float32'))
    x_test = theano.shared(np.asarray(_x_test, dtype='float32'))
    y_train = T.cast(theano.shared(np.asarray(_y_train, dtype='float32')), 'int32')
    y_test = T.cast(theano.shared(np.asarray(_y_test, dtype='float32')), 'int32')

    print('  Building model...')
    t = time()
    x = T.matrix('x')
    y = T.ivector('y')
    is_train = T.iscalar('is_train')
    mlp = model.MLP(is_train, [_x_train.shape[1], 400, 400, 400, 400, 48], x)
    pyx = mlp.p_y_given_x
    NLL = -T.mean(T.log(pyx)[T.arange(y.shape[0]), y])
    # CE = T.nnet.categorical_crossentropy(pyx, y)
    cost = NLL + (0.00003 * mlp.L2)

    #learning_rate = T.scalar('learning rate')
    learning_rate = 0.01
    #gparams = [T.grad(cost, param) for param in mlp.params]
    updates = []
    momentum = 0.5
    for param in mlp.params:
        grad_this = T.grad(cost, param)
        param_update = theano.shared(param.get_value()*0., broadcastable=param.broadcastable)
        #grad_sum = theano.shared(param.get_value()*1., broadcastable=param.broadcastable)
        #updates.append([grad_sum, grad_sum + T.square(grad_this)])
        updates.append([param, param - learning_rate * param_update])
        updates.append([param_update, momentum*param_update + (1-momentum)*grad_this])
    #updates = [
    #    (param, param - learning_rate * (gparam_momentum)) for
    #    param, gparam in zip(mlp.params, gparams)
    #]

    batch_index = T.lscalar('batch_index')
    train_model = theano.function(
        inputs=[batch_index],
        outputs=[cost, mlp.L2],
        updates=updates,
        givens={
            x: x_train[batch_index * mlp.batch_size: (batch_index+1) * mlp.batch_size],
            y: y_train[batch_index * mlp.batch_size: (batch_index+1) * mlp.batch_size],
            #learning_rate: T.cast(mlp.learning_rate, 'float32'),
            is_train: np.cast['int32'](1)
        }
    )

    train_error = theano.function(
        inputs=[batch_index],
        outputs=mlp.errors(y),
        givens={
            x: x_train[batch_index * mlp.batch_size: (batch_index+1) * mlp.batch_size],
            y: y_train[batch_index * mlp.batch_size: (batch_index+1) * mlp.batch_size],
            is_train: np.cast['int32'](0)
        }
    )

    test_error = theano.function(
        inputs=[],
        outputs=mlp.errors(y),
        givens={
            x: x_test,
            y: y_test,
            is_train: np.cast['int32'](0)
        }
    )

    n_sample = _y_train.shape[0]
    best_error = 1.
    best_cost = 1e9
    epoch_waited = 0
    last_cost = np.inf
    mlp.learning_rate = 0.001
    mlp.batch_size = 256
    n_epoch = 99999
    error = test_error()
    print('  Model built. Time elapsed: %.2f secs' % (time()-t))
    print('  MLP:\n%s' % (str(mlp)))
    print('  Error before training: %.4f' % (error))
    print('  Training...')
    for epoch in range(n_epoch):
        t = time()
        batches = int(n_sample / mlp.batch_size)
        print('  Epoch %d - (batch_size, learning_rate) = (%d, %f)' %
            (epoch+1, mlp.batch_size, mlp.learning_rate)
        )
        costs = []
        for index in range(batches):
            cost, l2 = train_model(index)
            costs.append(cost)
        train_errors = [train_error(i) for i in range(batches)]
        Ein = np.mean(train_errors)
        Eout = test_error()
        cost_avg = np.mean(costs)
        #if (cost_avg / last_cost) > 1.0001:
        #    mlp.learning_rate /= 1.5
        #else:
        #    mlp.learning_rate *= 1.01

        last_cost = cost_avg
        print('    `--Ein: %.5f, Eout: %.5f, Cost: %.5f, L2: %.2f,\n\
                      time elapsed: %.2f secs.' % 
            (Ein, Eout, cost_avg, l2, ((time()-t)/1))
        )

        #if cost_avg < best_cost:
        #    best_cost = cost_avg
        #    epoch_waited = 0

        # if wait too long since last best cost happened, try SGD
        #if epoch_waited > 100:
        #    mlp.batch_size = 1
        #    mlp.learning_rate = 0.1

        if Eout < best_error:
            best_error = Eout
            if model_save_name is not None:
                mlp.save(model_save_name+'_fold'+str(fold_cnt))

        #_x_train, _y_train = shuffle(_x_train, _y_train)
        #x_train = theano.shared(np.asarray(_x_train, dtype='float32'))
        #x_test = theano.shared(np.asarray(_x_test, dtype='float32'))
        #y_train = T.cast(theano.shared(np.asarray(_y_train, dtype='float32')), 'int32')
        #y_test = T.cast(theano.shared(np.asarray(_y_test, dtype='float32')), 'int32')



