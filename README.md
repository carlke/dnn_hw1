# 討論紀錄
-----
### 10/6

1. 切folds - 5 folds or 10(每個類別越平均越好), 可參考[1]

2. 對每一個維度作normalize(-1到1)

3. Learning rate: 0.05, 0.01, 0.005, 0.001, 0.0001

4. 策略: 先用minibatch找到一組不錯的組合,再用SGD(每個batch裡面最好有每個class的data)

5. 參考
 - [1] [StratifiedKFold](http://scikit-learn.org/stable/modules/generated/sklearn.cross_validation.StratifiedKFold.html#sklearn.cross_validation.StratifiedKFold)
 - [2] [MLP example @ deeplearning.net](http://deeplearning.net/tutorial/mlp.html)
 - [3] [Practical Recommendations for Gradient-Based Training of Deep Architectures](http://arxiv.org/pdf/1206.5533v2.pdf)
 - [4] [Cost functions](http://stats.stackexchange.com/questions/154879/a-list-of-cost-functions-used-in-neural-networks-alongside-applications)
 - [5] [Cost functions 2](https://github.com/torch/nn/blob/master/doc/criterion.md)

一些建議/消息：

6. 可以不要用原本的sigmoid function，改成hyperbolic tangent或rectified linear unit (ReLU)

7. input vector有normalize會比較好

8. (重要)要使用dropout的技巧：在training的時候每一層隨機關掉50%的neurons，也就是把這些neurons出來的值設為0。
最後要把train好的model拿去使用的時候，把所有的neurons都打開
可以參考 http://www.cnblogs.com/tornadomeet/p/3258122.html

9. learning rate要dynamic變動，可能要參考一些資料?
可以參考 http://caffe.berkeleyvision.org/tutorial/solver.html 這裡的
【Rules of thumb for setting the learning rate α and momentum μ】

10. 可以使用32bit的float就好，跟64bit差別不會太大

11. 比較好的performance在大概4層或5層的hidden layers，每層100~1000個neurons

12. 最後output layer的計算很花時間


# 使用方法
-----
## merge.py
將.ark檔與.lab檔合併成一個檔案
    
    python merge.py 訓練資料檔 label檔 輸出檔名

## utils.py
 - loadData()

讀取合併的檔案, X是feature vector, Y是label, n2lab是label-id與label-string的mapping

    X, Y, n2lab, frameID = utils.loadData(mergedFile)

 - loadFeatureData()

讀取.ark檔
    
    ids, x = utils.loadFeatureData(arkFile)

## model.py
 - load()

讀取MLP的model file

    mlp = model.load(modelFile)

-----
## Example
    python merge.py fbank/train.ark label/train.lab fbank.merged    #合併"train.ark"與"train.lab"為"fbank.merged"
    python myMLP.py                                                 #用"fbank.merged"來train MLP
    
# TODO
 - 檢查看看為何在server上跑和本機跑結果差那麼多
 - 試試看其他cost function
 - dropout
 - momentum
 - adaptive learning rate
 - adaptive batch size



