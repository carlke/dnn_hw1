"""
Contains classes like hiddenLayer, MLP. Also functions relate to them.

"""
import theano
import theano.tensor as T
import numpy as np

from sklearn.utils import shuffle
from sklearn.externals import joblib


def relu(x):
    return T.switch(x < 0, 0, x)

rng = np.random.RandomState()
srng = T.shared_randomstreams.RandomStreams()
class hiddenLayer(object):
    def __init__(self, is_train, n_in, n_out, input=None,
                 activation=relu, layer_name=None,
                 input_name=None, dropout=0.25):

        #rng = np.random.RandomState()

        _W = np.asarray(rng.uniform(
            low=-np.sqrt(6./(n_in + n_out)),
            high=np.sqrt(6./(n_in + n_out)),
            size=(n_in, n_out)
        ), dtype='float32')

        _b = np.asarray(np.zeros((n_out,), dtype='float32'))

        W = theano.shared(_W)
        b = theano.shared(_b)

        self.n_in = n_in
        self.n_out = n_out
        self.W = W
        self.b = b
        self.input = input
        self.L2 = (self.W ** 2).sum()
        self.activation = activation
        self.layer_name = layer_name
        self.input_name = input_name
        self.params = [self.W, self.b]

        #srng = T.shared_randomstreams.RandomStreams()
        mask = srng.binomial((n_out,), p=(1-dropout), dtype='float32')
        if self.input is not None:
            output = T.dot(self.input, self.W) + self.b
            output = (output if self.activation is None else self.activation(output))
            train_output = mask * output * np.cast['float32'](1./(1-dropout))
            self.output = T.switch(T.neq(is_train, 0), train_output, output)

    def __repr__(self):
        if self.activation is None:
            activation_name = 'linear'
        elif self.activation is T.tanh:
            activation_name = 'tanh'
        elif self.activation is relu:
            activation_name = 'ReLu'
        else:
            activation_name = str(self.activation)
        description = "    [ %-14s ], (%-3d, %-3d), Activation=\"%s\", Input: %s" % \
            (self.layer_name, self.n_in, self.n_out, activation_name, self.input_name)
        return description

    def set_input(self, input):
        self.input = input
        output = T.dot(self.input, self.W) + self.b
        self.output = (output if self.activation is None else self.activation(output))

    def set_activation(self, activation):
        self.activation = activation
        output = T.dot(self.input, self.W) + self.b
        self.output = (output if self.activation is None else self.activation(output))


class MLP(object):
    def __init__(self, is_train, dims, input, learning_rate=0.01, batch_size=50):
        if len(dims) < 2:
            raise Exception("Must specify at least 1 hidden unit number.\
             (Input number doesn't count)")
        layers = []
        if len(dims) > 2:
            layers.append(hiddenLayer(is_train, dims[0], dims[1],
            input=input, layer_name='InputLayer', input_name='From data'))
        else:
            layers.append(hiddenLayer(is_train, dims[0], dims[1],
            input=input, activation=None, layer_name='InputLayer', input_name='From data'))
        for i in range(1, len(dims)-2):
            layers.append(hiddenLayer(is_train, dims[i], dims[i+1],
            input=layers[i-1].output, layer_name='HiddenLayer-'+str(i),
            input_name=layers[i-1].layer_name+"'s output"))
        if len(dims) > 2:
            layers.append(hiddenLayer(is_train, dims[-2], dims[-1],
            input=layers[-1].output, activation=None, layer_name='OutputLayer',
            input_name=layers[-1].layer_name+"'s output"))

        self.layers = layers
        self.input = input
        self.output = layers[-1].output
        self.p_y_given_x = softmax(self.output)
        self.pred = T.argmax(self.p_y_given_x, axis=1)
        self.is_train = is_train

        # not sure if these two should be put in MLP instance
        self.learning_rate = learning_rate
        self.batch_size = batch_size

        self.params = []
        for l in self.layers:
            self.params += l.params

        self.L2 = self.layers[0].L2
        for i in range(1, len(self.layers)):
            self.L2 += self.layers[i].L2

    """
    def train(self, X, Y, cost, learning_rate=0.01):
        n_sample = Y.shape[0]
        batches = n_sample / batch_size
        indices = range(n_sample)
        indices_shuffled = shuffle(indices)

        gparams = [T.grad(cost, param) for param in self.params]
        updates = [
            (param, param - self.learning_rate * gparam) for
            param, gparam in zip(self.params, gparams)
        ]
        batch_index = T.lscalar()
        train_model = theano.function([batch_index], cost,
            updates=updates,
            givens={
                x: X[batch_index * batch_size: (batch_index+1) * batch_size],
                y: Y[batch_index * batch_size: (batch_index+1) * batch_size]
            }
        )

        for i in :
            self.train_model(i, )
    """
    def errors(self, y):
        if y.ndim != self.pred.ndim:
            raise TypeError('y.shape != pred.shape')
        if y.dtype.startswith('int'):
            return T.mean(T.neq(self.pred, y))
        else:
            raise TypeError('y.dtype != int')

    def predict(self, x):
        fpred = theano.function(
            inputs=[self.input],
            outputs=T.cast(self.pred, dtype='int32'),
            givens={
                self.is_train: np.cast['int32'](0)
            }
        )
        return fpred(x)

    def save(self, filename):
        joblib.dump(self, filename)

    def load(self, filename):
        mlp = joblib.load(filename)
        for attr in dir(mlp):
            if '__' not in attr:
                self.__setattr__(attr, mlp.__getattribute__(attr))


    def __repr__(self):
        description = "\n"
        for l in self.layers:
            description += "%s\n" % str(l)
        return description

def load(filename):
    mlp = MLP(0, [1,1], T.matrix())
    mlp.load(filename)

    # pre-compile fpred at load time
    try:
        mlp.predict([1])
    except:
        pass

    return mlp

def predict(filename, x):
    mlp = joblib.load(filename)
    fpred = theano.function(
        inputs=[mlp.input],
        outputs=T.cast(mlp.pred, dtype='int32'),
        givens={
            mlp.is_train: np.cast['int32'](0)
        }
    )
    return fpred(x)

def softmax(M):
    Mexp = T.exp(M)
    return Mexp / Mexp.sum(axis=1).reshape((Mexp.shape[0], 1))

